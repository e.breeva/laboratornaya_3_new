from operator import length_hint
import graph as gr
import requests
import copy
class filtr:
        def __init__(self, n):
            self.n=int(n)
            self.item=[]
            self.rez=None
            
        def add_item(self, flag,otkuda):
                    file=''
                    if int(flag)==1:
                        file=requests.get(otkuda).text
                    
                    if int(flag)==2:
                        file=open(otkuda)
                    for l in file:
                           self.item.append(float(l.rstrip()))            
                    if int(flag)==2:
                        file.close()                        
                    if len(self.item)>=25:
                        return(self.item)
                    else:
                        self.item=[]
                        print("ne dostatochno dannih")
                        return(self.item)

        
class mediana(filtr):
      
    def med(self):
            s1=copy.deepcopy(self.item)
            self.rez=[]
            for i in range(len(s1)-self.n):
                j=0
                window=[]
                while j<self.n:
                    window.append(s1[i+j])
                    j+=1
                window=sorted(window)
                m = int(length_hint(window) / 2)
                middle = window[m]
                self.rez.append(middle)
             
            return(self.rez)
        
class SMA(filtr):
               
        def sma(self):
            self.rez=copy.deepcopy(self.item)
            i = length_hint(self.rez)-1
            while i>=self.n:
                j=0
                temp=0
                while j<self.n:
                    temp+=self.rez[i-j]
                    j+=1            
                self.rez[i]=temp/self.n
                i-=1
            return(self.rez)
        
def mainSMA(n):
    sMa=SMA(n)
    sMa.add_item(2,"1.txt")
    r1=sMa.sma()
    gr.graphic(sMa.item,r1,"SMA")
    return(r1)
def mainMedian(n):
    mEd=mediana(n)
    mEd.add_item(2,"1.txt")
    r2=mEd.med()
    gr.graphic(mEd.item,r2,"median")
    
    return(r2)
#n=int(input("временной период(размер окна) = "))
#print(mainMedian(3))
##print(mainSMA(3))



